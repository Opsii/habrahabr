//
//  HHPost.h
//  
//
//  Created by Vinnichenko Dmitry on 6/26/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface HHPost : NSManagedObject

@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * postId;

@end
