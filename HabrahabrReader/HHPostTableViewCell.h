//
//  HHPostTableViewCell.h
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/26/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHPostTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *date;

- (void)setupPostWith:(HHPost*)post;

@end
