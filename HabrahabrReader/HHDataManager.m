//
//  HHDataManager.m
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/25/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import "HHDataManager.h"

@implementation HHDataManager

static HHDataManager* manager = nil;

#pragma mark - Init -

+ (HHDataManager*)defaultManager {
    if(!manager) {
        manager = [[HHDataManager alloc] init];
    }
    return manager;
}

#pragma mark - Public -

- (NSArray*)getDBNews {
    return [[HHPost MR_findAllSortedBy:@"postId" ascending:YES] mutableCopy];
}

- (void)deleteDBNews {
    for (HHPost* oldPost in [self getDBNews]) {
        [oldPost MR_deleteEntity];
        [self saveDBNews];
    }
}

- (void)saveDBNews {
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
}

@end
