//
//  HHNetworkManager.m
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/25/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import "HHNetworkManager.h"
#import <Reachability.h>

#define kHost @"http://habrahabr.ru/"
#define kNews @"rss/hubs/"

static HHNetworkManager* manager = nil;

static NSString* const HHConnectionErrorMessage = @"Problem with an internet connection";

static NSString* const kDate  = @"pubDate";
static NSString* const kTitle = @"title";
static NSString* const kItem  = @"item";
static NSString* const kLink  = @"link";


@interface HHNetworkManager () <NSXMLParserDelegate>
    
@property (strong, nonatomic) NSXMLParser* parser;

@property (strong, nonatomic) HHPost* bufferPost;
@property (strong, nonatomic) NSString *element;

@property (nonatomic) NSUInteger postId; // For sequence

@end

@implementation HHNetworkManager

#pragma mark - Init -

+ (HHNetworkManager*)defaultManager {
    if(!manager) {
        manager = [[HHNetworkManager alloc] init];
    }
    return manager;
}

#pragma mark - Public -

- (void)getLastNewsWithCompletition:(arrayBlock)block {
    
    /* Check connection */
    [self checkConnectionWithCompletition:^(NSError *error) {
        if (!error) {
            
            /* Parse */
            [self p_startParseWithCompletition:^(NSArray *result, NSError *error) {
                if (!error)
                    block(result,nil);
                else
                    block(nil,error);
            }];
        }
        else
            block(nil,error);
    }];
}

- (void)checkConnectionWithCompletition:(errorBlock)block {
    Reachability* internetReachableFoo = [Reachability reachabilityForInternetConnection];;
    NetworkStatus status = [internetReachableFoo currentReachabilityStatus];
    
    if(status == NotReachable) {
        NSError* error = [NSError errorWithDomain:@"local" code:555 userInfo:@{ NSLocalizedDescriptionKey:HHConnectionErrorMessage}];
        block(error);
    }
    else
        block(nil);
}

#pragma mark - Private -

- (void)p_startParseWithCompletition:(arrayBlock)block {
    [[HHDataManager defaultManager] deleteDBNews];
    
    NSURL* url = [NSURL URLWithString:[kHost stringByAppendingString:kNews]];
    [self p_initParserWithURL:url];
    
    
    if (![self.parser parse])
        block(nil, [self.parser parserError]);
    else
        block([[HHDataManager defaultManager] getDBNews], nil);
}

- (void)p_initParserWithURL:(NSURL*)url {
    self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    [self.parser setShouldResolveExternalEntities:NO];
    [self.parser setDelegate:self];
    
    self.postId = 0;
}

- (NSString*)p_cleanString:(NSString*)string {
    string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    return string;
}

#pragma mark - <NSXMLParserDelegate> -

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    self.element = elementName;
    
    /* Init post */
    if ([self.element isEqualToString:kItem])
        self.bufferPost = [HHPost MR_createEntity];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    NSString* infoString = [self p_cleanString:string];
    
    /* Setup post */
    if (![infoString isEqual:@""]) {
    
        if ([self.element isEqualToString:kTitle])
            self.bufferPost.title = infoString;
        else if ([self.element isEqualToString:kLink])
            self.bufferPost.link = infoString;
        else if ([self.element isEqualToString:kDate])
            self.bufferPost.date = infoString;
        
        self.bufferPost.postId = [NSNumber numberWithInteger:self.postId];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    /* Delete old posts n save new*/
    if ([elementName isEqualToString:kItem]) {
        self.postId ++;
        [[HHDataManager defaultManager] saveDBNews];
    }
}

@end
