//
//  HHNetworkManager.h
//  HabrahabrReader
//
//  Created by Vinnichenko Dmitry on 6/25/15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^arrayBlock) (NSArray* result, NSError* error);
typedef void (^errorBlock) (NSError* error);

@interface HHNetworkManager : NSObject

+ (HHNetworkManager*)defaultManager;

- (void)getLastNewsWithCompletition:(arrayBlock)block;

- (void)checkConnectionWithCompletition:(errorBlock)block;

@end
